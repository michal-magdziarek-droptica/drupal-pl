/**
 * @file
 * File with main tasks.
 */

(function () {

  'use strict';

  const fs = require('fs');
  const notifier = require('../helpers/notifier.js');
  const enums = require('../helpers/enums.js');

  module.exports = function (gulp, config, { compile, watch }, browserSync) {

    /**
     * Task for initializing browser sync with settings for website.
     */
    const browserSyncSite = (done) => {
      browserSync.init({
        injectChanges: true,
        proxy: config.site,
        host: config.site,
        notify: config.browserSync.notify,
        ui: config.browserSync.ui,
        open: config.browserSync.openBrowserAtStart,
        reloadOnRestart: config.browserSync.reloadOnRestart,
        port: config.browserSync.port,
      });

      done();
    };
    gulp.task(enums.mainTasksEnum.browserSyncSite, browserSyncSite);

    /**
     * Task for compiling site and running browser sync for it.
     */
    const serveSite = () => {};
    serveSite.description = 'Runs compile task and then initializes browser sync for site.';
    gulp.task(enums.mainTasksEnum.serveSite, gulp.series(enums.defaultTasksEnum.compile, enums.mainTasksEnum.browserSyncSite, enums.defaultTasksEnum.watch, serveSite));

    /**
     * Task for creating dist directory and symbolic link to parent dist in the sub theme.
     */
    const createDist = (done) => {
      const dir = config.themeDir + 'dist';
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      // Commented sub theme behavior due to bug with TS generation.
      // if (config.isSubtheme && !fs.existsSync(config.themeDir + 'parent-dist')) {
      //   notifier.sh(`ln -s  ${config.parentDir}dist ${config.themeDir}parent-dist`, false, () => {});
      // }

      done();
    };
    createDist.description = 'Creates dist directory if there is none.';
    gulp.task(enums.mainTasksEnum.dist, createDist);

    /**
     * Task for building everything.
     */
    gulp.task(enums.mainTasksEnum.build, gulp.series(enums.mainTasksEnum.dist, enums.defaultTasksEnum.compile));
  };

})();
