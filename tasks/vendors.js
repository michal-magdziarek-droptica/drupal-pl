/**
 * @file
 * File with vendor tasks.
 */

(function () {

  'use strict';

  const _ = require('lodash');
  const del = require('delete');
  const fs = require('fs');
  const enums = require('../helpers/enums.js');

  module.exports = function (gulp, config, { compile }) {
    const vendorConfig = config.vendorConfig;

    /**
     * Update the vendors directory.
     *
     * @param paths
     *   Object with src and dest paths.
     */
    const generateVendors = (paths) => {
      // Delete the directory.
      if (fs.existsSync(paths.dest)) {
        del.sync([paths.dest], {force: true});
      }

      // Read package.json.
      const buffer = fs.readFileSync(paths.src + 'package.json');
      const packageJson = JSON.parse(buffer.toString());

      // Quit early if there are no dependencies.
      if (_.isEmpty(packageJson.dependencies)) {
        return;
      }

      // Create the directory then copy JS and CSS files.
      fs.mkdirSync(paths.dest);
      for (let lib in packageJson.dependencies) {
        let dir = paths.dest + lib;
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir, { recursive: true });
        }
        gulp.src([paths.src + 'node_modules/' + lib + '/**/*.{js, css}']).pipe(gulp.dest(dir));
      }
    };

    /**
     * Task for copying libraries JS and CSS files from node_modules.
     */
    const vendorsGenerate = (done) => {
      generateVendors(vendorConfig.paths);
      if (config.isSubtheme) {
        generateVendors(vendorConfig.parentPaths);
      }

      done();
    };
    vendorsGenerate.description = 'Copies JS and CSS library files listed as dependencies in package.json from node_modules to dist/vendor.';
    gulp.task(enums.vendorsTasksEnum.generate, vendorsGenerate);

    // Push tasks names to corresponding arrays.
    compile.push(enums.vendorsTasksEnum.generate);
  };

})();
