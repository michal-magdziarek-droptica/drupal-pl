/**
 * @file
 * File with CSS tasks.
 */

(function() {

  'use strict';

  const sass = require('gulp-sass')(require('sass'));
  const sassGlob = require('gulp-sass-glob');
  const gulpif = require('gulp-if');
  const cleanCSS = require('gulp-clean-css');
  const cached = require('gulp-cached');
  const flatten = require('gulp-flatten');
  const sourcemaps = require('gulp-sourcemaps');
  const stylelint = require('gulp-stylelint');
  const prefix = require('gulp-autoprefixer');
  const nodeSassGlobbing = require('node-sass-globbing');
  const enums = require('../helpers/enums.js');

  module.exports = function (gulp, config, { compile, watch, validate }, browserSync) {
    const cssConfig = config.cssConfig;

    /**
     * Task for CSS compilation.
     */
    const cssCompile = (done) => {
      gulp.src(cssConfig.src)
        .pipe(cached('compiling'))
        .pipe(sassGlob())
        .pipe(gulpif(cssConfig.lint.enabled, stylelint({
          failAfterError: cssConfig.lint.failOnError,
          reporters: cssConfig.lint.reporters,
        })))
        .pipe(sourcemaps.init())
        .pipe(
          sass({
            importer: nodeSassGlobbing,
            outputStyle: cssConfig.outputStyle,
            sourceComments: cssConfig.sourceComments,
            includePaths: cssConfig.includePaths,
          }).on("error", sass.logError)
        )
        .pipe(prefix(cssConfig.autoPrefixerBrowsers))
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write((cssConfig.sourceMapEmbed) ? null : './'))
        .pipe(gulpif(cssConfig.flattenDestOutput, flatten()))
        .pipe(gulp.dest(cssConfig.dest))
        .on('end', () => {
          browserSync.reload('*.css');
          done();
        });
    };
    cssCompile.description = 'Compiles SCSS to CSS.';
    gulp.task(enums.cssTasksEnum.compile, cssCompile);

    /**
     * Task for CSS validation.
     */
    const cssValidate = () => {
      // Change behavior if there is background-check flag.
      if (process.argv.indexOf("--background-check") > -1) {
        cssConfig.lint.failOnError = true;
        cssConfig.lint.reporters = [];
      }

      return gulp.src(cssConfig.watchSrc)
        .pipe(cached(enums.cssTasksEnum.validate))
        .pipe(stylelint({
          failAfterError: cssConfig.lint.failOnError,
          reporters: cssConfig.lint.reporters,
        }));
    };
    cssValidate.description = 'Checks SCSS validation with Lint.';
    gulp.task(enums.cssTasksEnum.validate, cssValidate);

    /**
     * Task for CSS watcher.
     */
    const cssWatch = () => {
      const tasks = [enums.cssTasksEnum.compile];
      if (cssConfig.lint.enabled) {
        tasks.push(enums.cssTasksEnum.validate);
      }
      gulp.watch(cssConfig.watchSrc, cssConfig.watchOptions, gulp.series(...tasks));
    };
    cssWatch.description = 'Watches for changes in SCSS files.';
    gulp.task(enums.cssTasksEnum.watch, cssWatch);

    // Push tasks names to corresponding arrays.
    compile.push(enums.cssTasksEnum.compile);
    validate.push(enums.cssTasksEnum.validate);
    watch.push(enums.cssTasksEnum.watch);
  };

})();
