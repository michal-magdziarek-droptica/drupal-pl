/**
 * @file
 * File with image tasks.
 */

(function () {

  'use strict';

  const imagemin = require('gulp-imagemin');
  const notifier = require('../helpers/notifier.js');
  const enums = require('../helpers/enums.js');

  module.exports = function (gulp, config, { compile }) {
    const imagesConfig = config.imagesConfig;

    /**
     * Task for images minify.
     */
    const imagesMinify = (done) => {
      // Copy images from parent.
      if (config.isSubtheme) {
        notifier.sh(`while true; do echo n; done | cp -Ri ${config.parentDir}/images/ ${config.themeDir} 2>/dev/null`, false, () => {})

        // This below was simpler solution to the command above, but POSTIX does not have -n param for cp command.
        // notifier.sh(`cp -rn ${config.parentDir}/images/. ${config.themeDir}/images/`, false, () => {});
      }

      gulp.src(imagesConfig.src)
        .pipe(
          imagemin([
            imagemin.svgo({
              plugins: [{ removeViewBox: false }, { cleanupIDs: false }],
            }),
          ])
        )
        .pipe(gulp.dest(file => file.base));

      done();
    };
    imagesMinify.description = 'Copies missing files from parent theme and minify images.';
    gulp.task(enums.imagesTasksEnum.minify, imagesMinify);

    /**
     * Task for creating SVG sprite.
     */
    const svgSprite = (done) => {
      const svgSprite = require('gulp-svg-sprite');

      gulp.src(imagesConfig.iconsSrc)
        .pipe(svgSprite(imagesConfig.sprite))
        .pipe(gulp.dest('.'));

      done();
    };
    svgSprite.description = 'Creates sprite from SVG icons.';
    gulp.task(enums.imagesTasksEnum.sprite, svgSprite);

    // Push tasks names to corresponding arrays.
    compile.push(enums.imagesTasksEnum.minify);
    compile.push(enums.imagesTasksEnum.sprite);
  };

})();
