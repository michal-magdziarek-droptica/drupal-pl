/**
 * @file
 * File with JS tasks.
 */

(function () {
  'use strict';

  const uglify = require('gulp-uglify');
  const babel = require('gulp-babel');
  const cached = require('gulp-cached');
  const prettyError = require('gulp-prettyerror');
  const ts = require('gulp-typescript');
  const rename = require('gulp-rename');
  const enums = require('../helpers/enums.js');

  module.exports = function (gulp, config, { compile, watch }, browserSync) {
    const jsConfig = config.jsConfig;

    /**
     * Generate minified JS files.
     *
     * @param paths
     *   Object with src and dest paths.
     */
    const scriptsGenerate = ({ src, dest }, done) => {
      // Generate minified JS files.
      return gulp
        .src(src)
        .pipe(cached('scripts'))
        .pipe(prettyError())
        .pipe(
          ts({
            allowJs: true,
          }),
        )
        .pipe(
          babel({
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-class-properties'],
          }),
        )
        .pipe(uglify())
        .pipe(
          rename({
            suffix: '.min',
            dirname: '.',
          }),
        )
        .pipe(gulp.dest(dest))
        .on('end', () => {
          done();
        });
    };

    /**
     * Task for JS files generation.
     */
    const jsGenerate = (done) => {
      scriptsGenerate(jsConfig.paths, done);
    };
    jsGenerate.description = 'Generate output JS files and move them to dist directory.';
    gulp.task(enums.jsTasksEnum.generate, jsGenerate);


    /**
     * Task for JS watcher.
     */
    const jsWatch = () => {
      const tasks = [enums.jsTasksEnum.generate];
      gulp.watch(jsConfig.watchSrc, jsConfig.watchOptions, gulp.series(...tasks));
    };
    jsWatch.description = 'Watch for changes in JS files.';
    gulp.task(enums.jsTasksEnum.watch, jsWatch);

    // Push tasks names to corresponding arrays.
    compile.push(enums.jsTasksEnum.generate);
    watch.push(enums.jsTasksEnum.watch);
  };
})();
