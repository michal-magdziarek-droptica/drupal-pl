/**
 * @file
 * File with names for tasks.
 */

module.exports = {
  cssTasksEnum: {
    compile: 'css',
    validate: 'validate:css',
    watch: 'watch:css',
  },
  jsTasksEnum: {
    generate: 'js',
    watch: 'watch:js',
  },
  vendorsTasksEnum: {
    generate: 'vendors',
  },
  imagesTasksEnum: {
    minify: 'images',
    sprite: 'svg',
  },
  mainTasksEnum: {
    browserSyncSite: 'browser-sync:site',
    serveSite: 'serve:site',
    dist: 'create-dist',
    build: 'build',
  },
  defaultTasksEnum: {
    compile: 'compile',
    validate: 'validate',
    watch: 'watch',
  },
};
