/**
 * @file
 * Main configuration.
 */

(function () {

  /**
   * Gets parent theme name.
   *
   * @return string
   *   Name of the parent or empty string if there is none.
   */
  const getParentName = function () {
    const path = require('path');
    const yaml = require('js-yaml');
    const fs = require('fs');

    const currentDir = path.resolve('./');
    const themeName = currentDir.split('/').slice(-1).pop();
    const info = yaml.safeLoad(fs.readFileSync(themeName + '.info.yml', 'utf8'));

    let parentName = '';
    if (info.hasOwnProperty('base theme')) {
      parentName = info['base theme'];
    }

    return parentName;
  };

  const parentThemeName = getParentName();
  const isSubtheme = (parentThemeName !== '' && parentThemeName !== 'stable' && parentThemeName !== 'stable9');
  const themeDir = './';
  const parentDir = themeDir + '../' + parentThemeName + '/';

  let config = {
    site: '',
    themeDir: themeDir,
    parentDir: parentDir,
    isSubtheme: isSubtheme,
    vendorConfig: {
      enabled: true,
      paths: {
        src: themeDir,
        dest: themeDir + 'dist/vendor/',
      },
      parentPaths: {
        src: '',
        dest: '',
      },
    },
    cssConfig: {
      enabled: true,
      src: [
        themeDir + 'components/_patterns/**/*.scss',
      ],
      watchSrc: [
        themeDir + 'components/_patterns/**/*.scss',
      ],
      dest: themeDir + 'dist/',
      flattenDestOutput: true,
      lint: {
        enabled: false,
        failOnError: false,
        reporters: [{
          formatter: 'string',
          console: true,
        }],
      },
      sourceComments: false,
      sourceMapEmbed: false,
      outputStyle: 'expanded',
      autoPrefixerBrowsers: ['last 2 versions', 'IE >= 11'],
      includePaths: [
        themeDir + 'node_modules',
      ],
      watchOptions: {},
    },
    jsConfig: {
      enabled: true,
      paths: {
        src: themeDir + 'components/_patterns/**/*.@(js|ts)',
        dest: themeDir + 'dist/js/',
      },
      parentPaths: {
        src: '',
        dest: '',
      },
      watchSrc: [
        themeDir + 'components/_patterns/**/*.js',
        themeDir + 'components/_patterns/**/*.ts',
      ],
      lint: {
        enabled: false,
        failOnError: false,
      },
      watchOptions: {},
    },
    imagesConfig: {
      enabled: true,
      src: themeDir + 'images/**/*',
      iconsSrc: themeDir + 'images/icons/src/**/*.svg',
      sprite: {
        mode: {
          symbol: {
            dest: 'dist/img/sprite',
            sprite: 'sprite.svg',
            example: false,
          },
        },
        svg: {
          xmlDeclaration: false,
          doctypeDeclaration: false,
        },
      },
    },
    browserSync: {
      enabled: true,
      notify: false,
      openBrowserAtStart: false,
      reloadOnRestart: true,
      ui: false,
      port: '3000',
    },
  };

  if (isSubtheme) {
    // Add vendor paths.
    config.vendorConfig.parentPaths.src = parentDir;
    config.vendorConfig.parentPaths.dest = parentDir + 'dist/vendor/';

    // Add CSS paths.
    config.cssConfig.watchSrc.push(parentDir + 'components/_patterns/**/*.scss');
    config.cssConfig.includePaths.push(parentDir + 'node_modules');

    // Add JS paths.
    config.jsConfig.parentPaths.src = parentDir + 'components/_patterns/**/*.js';
    config.jsConfig.parentPaths.dest = parentDir + 'dist/js/';
    config.jsConfig.watchSrc.push(parentDir + 'components/_patterns/**/*.js');
  }

  module.exports = config;

})();
