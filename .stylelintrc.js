module.exports = {
  extends: [
    'stylelint-config-idiomatic-css',
  ],
  overrides: [
    {
      files: [
        '*.scss',
        '**/*.scss',
      ],
      customSyntax: require('postcss-scss'),
    },
  ],
  plugins: [
    'stylelint-scss',
  ],
  rules: {
    'block-closing-brace-empty-line-before': 'never',
    'block-opening-brace-newline-after': 'always',
    'rule-empty-line-before': [
      'always',
      {
        except: [
          'after-single-line-comment',
          'first-nested',
        ],
      },
    ],
    'property-no-unknown': true,
    'no-duplicate-selectors': true,
    'shorthand-property-no-redundant-values': true,
    indentation: 2,
    'declaration-block-no-duplicate-properties': true,
    'scss/at-mixin-argumentless-call-parentheses': 'never',
    'scss/selector-no-redundant-nesting-selector': true,
    'scss/dollar-variable-colon-space-after': 'at-least-one-space',
    'scss/at-rule-no-unknown': true,
    'scss/comment-no-empty': true,
    'scss/dollar-variable-first-in-block': [
      true,
      {
        ignore: [
          'imports',
          'comments',
        ],
      },
    ],
    'scss/dollar-variable-no-missing-interpolation': true,
    'scss/dollar-variable-empty-line-after': [
      'always',
      {
        except: [
          'before-dollar-variable',
          'last-nested',
        ],
      },
    ],
    'no-eol-whitespace': true,
  },
};
