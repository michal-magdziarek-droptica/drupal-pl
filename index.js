/**
 * @file
 * Main library file.
 */

(function () {

  'use strict';

  module.exports = function (gulp, localConfig) {

    const _ = require('lodash');
    const browserSync = require('browser-sync').create();
    const defaultConfig = require('./config/default-config');
    let config = _.defaultsDeep(localConfig, defaultConfig);

    const tasks = {
      compile: [],
      watch: [],
      validate: [],
      default: [],
    };

    // Vendors.
    require('./tasks/vendors.js')(gulp, config, tasks);

    // Styles.
    require('./tasks/styles.js')(gulp, config, tasks, browserSync);

    // Scripts.
    require('./tasks/scripts.js')(gulp, config, tasks, browserSync);

    // Images.
    require('./tasks/images.js')(gulp, config, tasks);

    gulp.task('compile', gulp.series(...tasks.compile));
    gulp.task('validate', gulp.series(...tasks.validate));
    gulp.task('watch', gulp.parallel(...tasks.watch));

    // Main.
    require('./tasks/main.js')(gulp, config, tasks, browserSync);
  };

}());
